// import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

const Axios = require('Axios')

export default class ConsumeEndpointApisController {
    // index controller
    async index({ view }) {
        const result = await Axios.get("https://6205d489161670001741bd34.mockapi.io/link-aja/fetch")
        // console.log(result)

        // return result.
        return view.render('pages/result', {
            results: result.data
        })
    }
}
